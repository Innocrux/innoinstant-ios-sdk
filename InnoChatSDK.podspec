Pod::Spec.new do |s|
s.name              = 'InnoChatSDK'
s.version           = '1.0'
s.summary           = 'Description of InnoChatSDK Framework.'

s.description      = <<-DESC
A bigger description of InnoChatSDK Framework.
DESC

s.homepage          = 'https://www.innoinstant.com'
s.license           = "MIT"
s.authors           = { 'Jagen K' => 'jagen@innocrux.com' }
s.source            = { :git => "https://bitbucket.org/Innocrux/innoinstant-ios-test-sdk.git", :tag => s.version }
s.vendored_frameworks = 'InnoChatSDK.xcframework'
s.swift_version     = '5.5.2'

s.ios.deployment_target = '12.0'


# Add all the dependencies
s.dependency 'MQTTClient'
s.dependency 'RealmSwift', '~> 10.20.0'
s.dependency 'Alamofire'
s.dependency 'AlamofireImage'
s.dependency 'libPhoneNumber-iOS'
s.dependency 'ReachabilitySwift'
s.dependency 'Firebase/Crashlytics'
s.dependency 'IDZSwiftCommonCrypto'

s.pod_target_xcconfig = { 'SWIFT_VERSION' => '5.5.2' }

end